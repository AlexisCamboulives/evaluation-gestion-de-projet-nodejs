import { Request, Response } from "express-serve-static-core";
import HomeController from "./HomeController";

export default class ProjectController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const viewProjet = db.prepare('SELECT * FROM Project').all();

        res.render('pages/index', {
            viewProjet: viewProjet
        });
    }
    static showCreateForm(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        
        const client = db.prepare('SELECT * FROM Client').all();

        res.render('pages/project-create', {
            client: client
        });
    }
    static createProjet(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        db.prepare('INSERT INTO Project ("title", "content", "Client_idClient") VALUES (?, ?, ?)').run(req.body.title, req.body.content, req.body.clientSelect);
        ProjectController.index(req, res);
    }
    static readProjet(req: Request, res: Response): void
    {
        const db = req.app.locals.db
        const projet = db.prepare('SELECT * FROM Project WHERE idProject = ?').get(req.params.idProject);
        res.render('pages/project-read', {
            projet: projet,
        });
    }
    static showFormUpdateProjet(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const updateProjet = db.prepare('SELECT * FROM Project WHERE idProject = ?').get(req.params.idProject);
        const client = db.prepare('SELECT * FROM Client').all();

        res.render('pages/project-update', {
            title: 'Gestion de projet',
            updateProjet: updateProjet,
            client: client
        });
    }
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE Project SET title = ?, content = ?, Client_idClient = ? WHERE idProject = ?').run(req.body.title, req.body.content, req.body.clientSelect, req.params.idProject);

        ProjectController.index(req, res);
    }
    static delete(req: Request, res:Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM Project WHERE idProject = ?').run(req.params.idProject);

        ProjectController.index(req, res);
    }
}