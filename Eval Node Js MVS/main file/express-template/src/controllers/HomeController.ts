import { Request, Response } from "express-serve-static-core";
import ProjectController from "./ProjectController";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const viewProjet = db.prepare('SELECT * FROM Project').all();

        res.render('pages/index', {
            viewProjet: viewProjet
        });
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }
}