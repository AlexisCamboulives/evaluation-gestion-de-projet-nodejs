import { Request, Response } from "express-serve-static-core";
import HomeController from "./HomeController";

export default class DevController
{
    static devIndex(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const devView = db.prepare('SELECT * FROM dev').all();

        res.render('pages/dev', {
            title: 'Gestion de Projet',
            devView: devView
        });
    }
    static showFormDevCreation(req: Request, res: Response): void
    {
        res.render('pages/dev-create', {
            title: 'Ajouter un Developpeur',
        });
    }
    static devCreate(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        db.prepare('INSERT INTO dev ("firstName", "lastName", "level") VALUES (?, ?, ?)').run(req.body.firstName, req.body.lastName, req.body.level);
        HomeController.index(req, res);
    }
}