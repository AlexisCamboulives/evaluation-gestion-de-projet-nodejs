import { Application } from "express";
import DevController from "./controllers/DevController";
import HomeController from "./controllers/HomeController";
import ProjectController from "./controllers/ProjectController";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/index', (req, res) =>
    {
        ProjectController.index(req, res);
    });
    /** Gestion de projet **/
    /** Création de projet */
    app.get('/project-create', (req, res) => {
        ProjectController.showCreateForm(req, res);
    });
    app.post('/project-create', (req, res) => {
        ProjectController.createProjet(req, res);
    });

    /** Voir en détail un projet */
    app.get('/project-read/:idProject', (req, res) => {
        ProjectController.readProjet(req, res);
    });

    /** Modifier un projet */
    app.get('/project-update/:idProject', (req, res) => {
        ProjectController.showFormUpdateProjet(req, res);
    });
    app.post('/project-update/:idProject', (req, res) => {
        ProjectController.update(req, res);
    });
    /** Suprimmer des projets */
    app.get('/project-delete/:idProject', (req, res) => {
        ProjectController.delete(req, res);
    });
    /** Gestion des dev **/
    app.get('/dev', (req, res) => {
        DevController.devIndex(req, res);
    });
    app.get('/dev-create', (req, res) => {
        DevController.showFormDevCreation(req, res);
    });
    app.post('/dev-create', (req, res) => {
        DevController.devCreate(req, res);
    });
}
