-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Alexis
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 09:48
-- Created:       2021-12-20 09:33
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "Client"(
  "idClient" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "clientName" VARCHAR(45),
  "ClientAdresse" VARCHAR(45)
);
CREATE TABLE "dev"(
  "iddev" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "firstName" VARCHAR(45),
  "lastName" VARCHAR(45),
  "level" VARCHAR(45)
);
CREATE TABLE "Project"(
  "idProject" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45),
  "content" LONGTEXT,
  "Client_idClient" INTEGER,
  CONSTRAINT "fk_Project_Client1"
    FOREIGN KEY("Client_idClient")
    REFERENCES "Client"("idClient")
);
CREATE INDEX "Project.fk_Project_Client1_idx" ON "Project" ("Client_idClient");
CREATE TABLE "Project_has_dev"(
  "Project_idProject" INTEGER NOT NULL,
  "dev_iddev" INTEGER NOT NULL,
  PRIMARY KEY("Project_idProject","dev_iddev"),
  CONSTRAINT "fk_Project_has_dev_Project"
    FOREIGN KEY("Project_idProject")
    REFERENCES "Project"("idProject"),
  CONSTRAINT "fk_Project_has_dev_dev1"
    FOREIGN KEY("dev_iddev")
    REFERENCES "dev"("iddev")
);
CREATE INDEX "Project_has_dev.fk_Project_has_dev_dev1_idx" ON "Project_has_dev" ("dev_iddev");
CREATE INDEX "Project_has_dev.fk_Project_has_dev_Project_idx" ON "Project_has_dev" ("Project_idProject");
COMMIT;